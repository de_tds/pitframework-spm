// swift-tools-version:5.5
import PackageDescription

let package = Package(
    name: "PITFramework",
    products: [
        .library(
            name: "PITFramework",
            targets: ["PITFramework"]),
    ],
    targets: [
        .binaryTarget(
            name: "PITFramework",
            url: "https://gitlab.com/de_tds/pitframework-spm/-/releases/v1.13.0/downloads/PITFramework.xcframework.zip",
            checksum: "a3d2e636cf4578680d193c136b70aec6c324d2470f46e07e4bd4b69a8b2124f8"),
    ]
)